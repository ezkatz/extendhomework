The Extend Movies API sources data from themoviedb.org and stores a local copy of that data in ./extendhomework/data/movies.json. 
We then use a script called movie_analyzer.py to determine the top 20 movies by year and store the results in ./extendhomework/data/top_movies.json.

# Project Setup
To setup the project locally:
- Create the conda env: conda create --name extendhomework
- Activate the environment: source activate extendhomework
- Install the dependencies: conda install --file requirements.txt

To regenerate the requirements.txt:
pip list --format=freeze > requirements.txt

# Running the API
- Right click the application.py file 
- Select run 'application'
- Go to http://0.0.0.0:5000/apidocs

# Running the Tools
There are 2 scripts in tools to fetch the underlying data for the API:
- movie_downloader.py - used to fetch the full movie dataset from themoviedb.org
- movie_analyzer.py - used to run analytical queries on the movie dataset

In order to refresh the data that this API uses, you will need to run movie_downloader.py and then movie_analyzer.py. 
The data artifacts from each script is stored in ./extendhomework/data. 

# Check out the hosted project (on AWS)
- Swagger Page: http://extendhomework.eba-cfbjsu23.us-east-1.elasticbeanstalk.com/apidocs

# Deploying this app to AWS
This app is deployed via elasticbeanstalk. Simply, zip all the files and folders in the root directory and deploy 
the zip file via the AWS UI.   
