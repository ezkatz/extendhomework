import json
from datetime import datetime
from typing import List

import pandas as pd


class MoviesAnalyzer:

    def get_top_movies_by_year(self, top_n_movies, past_n_years, input_file, output_file):
        top_movies_by_year = {}
        df = pd.read_json(input_file, lines=True)
        df = df.sort_values('popularity', ascending=False)
        years_to_include = list(range(datetime.now().year-past_n_years+1, datetime.now().year+1))

        for index, item in df.iterrows():
            if type(item['release_date']) is not str:
                continue

            year = item['release_date'][0:4]
            if year == '' or int(year) not in years_to_include:
                continue

            if year not in top_movies_by_year:
                top_movies_by_year[year] = []

            if len(top_movies_by_year[year]) < top_n_movies:
                top_movies_by_year[year].append(json.dumps(item.to_dict()))

        if output_file != None:
            results = []

            for x in top_movies_by_year.values():
                for j in x:
                    results.append(j)

            self._write_to_file(output_file, results)
        else:
            return top_movies_by_year

    def _write_to_file(self, file_path: str, movies: List[str]):
        with open(file_path, 'w') as file:
            for line in movies:
                file.write(line + '\n')
                print(f"writing batch of {len(movies)} to {file_path}")

if __name__ == '__main__':
    MoviesAnalyzer().get_top_movies_by_year(20, 5, './extendhomework/data/movies.json', './extendhomework/data/top_movies.json')