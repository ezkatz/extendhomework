import json
import os
from typing import List

import requests

class MovieDownloader:

    MOVIEDBORG_URL = "https://api.themoviedb.org/3/movie/popular?api_key=aa0ea741dcbdabdf6fd9953b60e629cf&language=en-US"

    def download(self, output_file: str):
        if os.path.exists(output_file):
            os.remove(output_file)

        movies_response = requests.get(self.MOVIEDBORG_URL).json()
        self._write_to_file(output_file, movies_response['results'])

        total_pages = movies_response["total_pages"]
        for page_num in range(2,total_pages+1):
            movies_response = requests.get(f"{self.MOVIEDBORG_URL}&page={page_num}").json()
            self._write_to_file(output_file, movies_response['results'])

    def _write_to_file(self, file_path: str, movies: List[str]):
        with open(file_path, 'a') as file:
            for line in movies:
                file.write(json.dumps(line) + '\n')
                print(f"writing batch of {len(movies)} to {file_path}")

if __name__ == '__main__':
    MovieDownloader().download('./extendhomework/data/movies.json')