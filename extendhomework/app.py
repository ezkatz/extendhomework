import logging

from flask import Flask

from extendhomework.api import api
from extendhomework.extensions import EXTENSION_REGISTRY

logger = logging.getLogger(__name__)

def create_app() -> Flask:
    app = Flask(__name__)

    for ext in EXTENSION_REGISTRY:
        ext.init_app(app)

    app.register_blueprint(api, url_prefix='/')

    return app
