import json

from flask import Blueprint, request, Response
import pandas as pd

api = Blueprint("api", __name__)

df = pd.read_json('./extendhomework/data/top_movies.json', lines=True)

@api.route('/movies', methods=['GET'])
def get_movies():
    """
    file: ./docs/api/static/swagger/get-movies.yml
    """
    offset = request.args.get('offset', default=0, type=int)
    limit = request.args.get('limit', default=100, type=int)
    query = request.args.get('query', default=None, type=str)
    sort = request.args.get('sort', default=None, type=str)

    results = df
    total = len(df)

    if query is not None:
        results = results[results["title"].str.contains(query, case=False)]
        total = len(results)

    results = _apply_sort(sort, results)

    results = results.iloc[offset:offset+limit]

    return Response(response=json.dumps({"total_results": total, "limit":limit, "offset":offset,
                                         "movies": results[["id", "title", "release_date"]].to_dict(orient='records')}),
                    mimetype="application/json")

@api.route('/movies/<int:movie_id>', methods=['GET'])
def get_movies_with_id(movie_id):
    """
    file: ./docs/api/static/swagger/get-movies-with-id.yml
    """
    movie_details = df.loc[df['id'] == movie_id]

    if len(movie_details) == 0:
        return Response({}, mimetype="application/json")

    return Response(response=json.dumps(movie_details.to_dict(orient='records')[0]), mimetype="application/json")

def _apply_sort(sort, results):
    if sort == 'release_date_asc':
        results = results.sort_values(by='release_date', ascending=True)
    elif sort == 'release_date_asc':
        results = results.sort_values(by='release_date', ascending=False)

    return results