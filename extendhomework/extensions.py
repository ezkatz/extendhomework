from flasgger import Swagger

swag = Swagger(template={"schemes": ["http"], "info": {"title": "Extend Movie API"}})

EXTENSION_REGISTRY = [
    swag
]